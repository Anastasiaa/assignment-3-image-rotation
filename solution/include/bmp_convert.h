#ifndef IMAGE_TRANSFORMER_BMP_CONVERT_H
#define IMAGE_TRANSFORMER_BMP_CONVERT_H

#include "picture.h"
#include <stdbool.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status  {
    READ_OK = 0,
    READ_SIGNATURE_ERROR,
    READ_BITS_ERROR,
    READ_HEADER_ERROR,
    READ_PADDING_ERROR
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_BITS_ERROR,
    WRITE_SIGNATURE_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_DATA_ERROR,
    WRITE_PADDING_ERROR
};


enum read_status from_bmp(FILE* input_file, struct pict* picture);
enum write_status to_bmp(FILE* output_file, const struct pict* picture);

#endif //IMAGE_TRANSFORMER_BMP_CONVERT_H
