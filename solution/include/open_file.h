#ifndef IMAGE_TRANSFORMER_OPEN_FILE_H
#define IMAGE_TRANSFORMER_OPEN_FILE_H

#include <stdbool.h>
#include <stdio.h>

enum open_status{
    OPEN_OK,
    OPEN_ERROR
};

enum open_status open_file(const char* file_name, FILE** file, const char* type);

#endif //IMAGE_TRANSFORMER_OPEN_FILE_H
