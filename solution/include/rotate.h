#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "picture.h"

struct pict rotate_clocked(struct pict picture, struct pix* pixels, int64_t angle);

#endif //IMAGE_TRANSFORMER_ROTATE_H
