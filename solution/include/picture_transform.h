#ifndef IMAGE_TRANSFORMER_PICTURE_TRANSFORM_H
#define IMAGE_TRANSFORMER_PICTURE_TRANSFORM_H

#include <stdio.h>

#include "bmp_convert.h"
#include "close_file.h"
#include "open_file.h"
#include "rotate.h"

enum transform_status{
    UNSUPPORTED_ANGLE,
    FILE_OPEN_ERROR,
    FILE_READ_ERROR,
    FILE_CLOSE_ERROR,
    BMP_CONVERT_ERROR,
    TRANSFORM_OK = 0
};

enum transform_status transform (
        const char* source_file_name,
        const char* target_file_name,
        int64_t angle
        );

#endif //IMAGE_TRANSFORMER_PICTURE_TRANSFORM_H

