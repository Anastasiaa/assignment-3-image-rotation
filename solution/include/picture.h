#ifndef IMAGE_TRANSFORMER_PICTURE_H
#define IMAGE_TRANSFORMER_PICTURE_H

#include <stdint.h>
#include <stdio.h>

struct pix {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

struct pict {
    uint64_t width;
    uint64_t height;
    struct pix *data;
};

size_t find_index(size_t x, size_t y, size_t width);
struct pict none_picture(uint64_t width, uint64_t height);
struct pict new_picture(size_t width, size_t height, struct pix* pixels);
void free_picture(struct pict* picture);

#endif //IMAGE_TRANSFORMER_PICTURE_H
