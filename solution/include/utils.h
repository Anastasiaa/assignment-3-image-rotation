#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H

#include "bmp_convert.h"

size_t padding_size(size_t width);
bool head_read(FILE* file, struct bmp_header* header);
size_t picture_size(const struct pict* pict);
size_t file_size(const struct pict* pict);
struct bmp_header create_header(const struct pict* pict);

#endif //IMAGE_TRANSFORMER_UTILS_H
