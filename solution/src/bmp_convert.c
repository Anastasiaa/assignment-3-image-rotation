#include "bmp_convert.h"

#include "utils.h"

enum read_status from_bmp(FILE* input_file, struct pict* picture){
    struct bmp_header header = {0};

    if(!head_read(input_file, &header)){
        free_picture(picture);
        return READ_HEADER_ERROR;
    }
    if(header.bfType != 19778){
        free_picture(picture);
        return READ_SIGNATURE_ERROR;
    }

    *picture = none_picture(header.biWidth, header.biHeight);
    const size_t padding = padding_size(picture->width);

    for(size_t i = 0; i < picture->height; i++){
        for(size_t j = 0; j < picture->width; j++){
            if(!fread(&(picture->data[find_index(j, i, picture->width)]), sizeof(struct pix), 1, input_file)){
                free_picture(picture);
                return READ_BITS_ERROR;
            }
        }
        if(padding != 0) {
            if (fseek(input_file, (long) padding, SEEK_CUR)) {
                free_picture(picture);
                return READ_PADDING_ERROR;
            }
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* output_file, const struct pict* picture){
    struct bmp_header header = create_header(picture);

    if(!fwrite(&header, sizeof(struct bmp_header), 1, output_file)){
        return WRITE_HEADER_ERROR;
    }

    if(fseek(output_file, (long) header.bOffBits, SEEK_SET)){
        return WRITE_SIGNATURE_ERROR;
    }

    const uint8_t paddings[3] = {0};
    const size_t padding = padding_size(picture->width);

    if(picture->data == NULL){
        return WRITE_DATA_ERROR;
    }

    for(size_t i = 0; i < picture->height; i++){
        for(size_t j = 0; j < picture->width; j++) {
            if (!fwrite(&picture->data[find_index(j, i, picture->width)], sizeof(struct pix), 1, output_file)) {
                return WRITE_BITS_ERROR;
            }
        }
        if(padding != 0){
            if(!fwrite(paddings, padding, 1, output_file)){
                return WRITE_PADDING_ERROR;
            }
        }
    }
    return WRITE_OK;
}
