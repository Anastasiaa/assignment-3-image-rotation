#include "picture_transform.h"

#include <stdio.h>
#include <stdlib.h>


bool angle_validate(int64_t angle) {
    if (angle % 90 != 0 && (angle >= -270 && angle <= 270)) {
        perror("unsupported angel rotate_picture");
        return false;
    }
    return true;
}

int main(int argc, char** argv)
{
    if (4 != argc) {
        fprintf(stderr,
                "\nUsage: %s <source-image> <transformed-image> <angle>"
                "\nWhere <angle> = 0, 90, -90, 180, -180, 270, -270"
                "\n", argv[0]
        );
        return 1;
    }
    char* p_end = 0;

    int64_t angle = strtol (argv[3], &p_end, 10);
    if (!angle_validate(angle)) {
        return UNSUPPORTED_ANGLE;
    }
    return transform(argv[1], argv[2], angle);
}
