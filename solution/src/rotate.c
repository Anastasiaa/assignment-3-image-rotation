#include "rotate.h"

#include <memory.h>


void rotate_90(const struct pict *picture, struct pix *pixels) {
    for(size_t y = 0; y < picture->height; y++){
        for(size_t x = 0; x < picture->width; x++){
            pixels[find_index(y, picture->width - x - 1, picture->height)] = picture->data[find_index(x, y, picture->width)];
        }
    }
}

void rotate_180(const struct pict *picture, struct pix *pixels) {
    for(size_t y = 0; y < picture->height; y++){
        for(size_t x = 0; x < picture->width; x++){
            pixels[find_index(picture->width - x - 1, picture->height - y - 1, picture->width)] = picture->data[find_index(x, y, picture->width)];
        }
    }
}

void rotate_270(const struct pict *picture, struct pix *pixels) {
    for(size_t y = 0; y < picture->height; y++){
        for(size_t x = 0; x < picture->width; x++){
            pixels[find_index(picture->height - y - 1, x, picture->height)] = picture->data[find_index(x, y, picture->width)];
        }
    }
}

void rotate_0(const struct pict *picture, struct pix *pixels) {
    for(size_t i = 0; i < picture->height * picture->width; i++){
        pixels[i] = picture->data[i];
    }
}

struct pict rotate_clocked(const struct pict picture, struct pix* pixels, int64_t angle) {
    if(picture.data == NULL){
        return none_picture(picture.width, picture.height);
    }
    uint64_t width;
    uint64_t height;
    if (angle < 0) angle += 360;
    switch (angle%360) {
        case 90:
            rotate_90(&picture, pixels);
            width = picture.height;
            height = picture.width;
            break;
        case 180:
            rotate_180(&picture, pixels);
            width = picture.width;
            height = picture.height;
            break;
        case 270:
            rotate_270(&picture, pixels);
            width = picture.height;
            height = picture.width;
            break;
        case 0:
            rotate_0(&picture, pixels);
            width = picture.width;
            height = picture.height;
            break;
        default:
            return none_picture(picture.width, picture.height);
    }
    return new_picture(width, height, pixels);
}
