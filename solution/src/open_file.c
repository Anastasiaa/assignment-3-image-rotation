#include "open_file.h"

enum open_status open_file(const char* file_name, FILE** file, const char* type){
    *file = fopen(file_name, type);
    if(*file == NULL){
        return OPEN_ERROR;
    }
    return OPEN_OK;
}
