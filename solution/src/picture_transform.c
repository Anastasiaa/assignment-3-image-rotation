#include "picture_transform.h"

#include <stdlib.h>

enum transform_status transform (const char* source_file_name, const char* target_file_name, int64_t angle) {

    FILE *file;
    if (open_file(source_file_name, &file, "rb") != OPEN_OK) {
        perror("couldn't open the file");
        return FILE_OPEN_ERROR;
    }

    struct pict image = {0};

    if (from_bmp(file, &image) != READ_OK) {
        perror("couldn't convert from bmp");
        return FILE_READ_ERROR;
    }

    struct pix* pixels = malloc(sizeof(struct pix) * image.width * image.height);
    if (pixels == NULL) {
        return BMP_CONVERT_ERROR;
    }

    struct pict res = rotate_clocked(image, pixels, angle);

    FILE *res_file;

    if (open_file(target_file_name, &res_file, "wb") != OPEN_OK) {
        perror("couldn't open the file");
        free_picture(&res);
        return FILE_OPEN_ERROR;
    }

    if (to_bmp(res_file, &res) != WRITE_OK) {
        perror("couldn't convert to bmp");
        free_picture(&res);
        close_file(res_file);
        return BMP_CONVERT_ERROR;
    }

    if (close_file(file) != CLOSE_OK) {
        perror("couldn't close the file");
        free_picture(&image);
        return FILE_CLOSE_ERROR;
    }

    if (close_file(res_file) != CLOSE_OK) {
        perror("couldn't close the file");
        free_picture(&image);
        return FILE_CLOSE_ERROR;
    }

    free_picture(&image);
    free_picture(&res);


    return TRANSFORM_OK;
}
