#include "utils.h"

#define BF_TYPE 19778
#define BF_REVERSED 0
#define BF_OFFBITS 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_X_PELS_PER_METER 0
#define BI_Y_PELS_PER_METER 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0


size_t padding_size(const size_t width){
    return width % 4;
}

bool head_read(FILE* file, struct bmp_header* header){
    return fread(header, sizeof(struct bmp_header), 1, file);
}

size_t picture_size(const struct pict* pict){
    return (pict->width * sizeof(struct pix) + padding_size(pict->width)) * pict->height;
}

size_t file_size(const struct pict* pict){
    return picture_size(pict) + sizeof(struct bmp_header);
}

struct bmp_header create_header(const struct pict* pict){
    return (struct bmp_header){
            .bfType = BF_TYPE,
            .bfileSize = file_size(pict),
            .bfReserved = BF_REVERSED,
            .bOffBits = BF_OFFBITS,
            .biSize = BI_SIZE,
            .biWidth = pict->width,
            .biHeight = pict->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT, //quantity of bits in bpm file
            .biCompression = BI_COMPRESSION,
            .biSizeImage = picture_size(pict),
            .biXPelsPerMeter = BI_X_PELS_PER_METER,
            .biYPelsPerMeter = BI_Y_PELS_PER_METER,
            .biClrImportant = BI_CLR_USED,
            .biClrUsed = BI_CLR_IMPORTANT
    };
}
