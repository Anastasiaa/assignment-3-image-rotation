#include "close_file.h"

enum close_status close_file(FILE* file){
    if(fclose(file) == EOF){
        return CLOSE_ERROR;
    }
    return CLOSE_OK;
}
