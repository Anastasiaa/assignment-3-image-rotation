#include "picture.h"

#include <stdint.h>
#include <stdlib.h>

size_t find_index(const size_t x, const size_t y, const size_t width){
    return y * width + x;
}

struct pict none_picture(const uint64_t width, const uint64_t height){
    struct pix* pixels = malloc(sizeof(struct pix) * height * width);
    return new_picture(width, height, pixels);
}

struct pict new_picture(const size_t width, const size_t height, struct pix* pixels){
    return (struct pict) {.data = pixels, .width = width, .height = height};
}

void free_picture(struct pict* picture){
    free(picture->data);
    picture = NULL;
}
